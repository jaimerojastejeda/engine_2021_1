#include "BaseObject.h"
	

BaseObject::BaseObject()
{
	this->id = ResourceManager::getInstance()->getNextId();
	this->nombre = "BaseObject";
	this->position = vector2d(0,0);
	this->escala = vector2d(1,1);
	this->rotacion = 0.0f;
	this->transform = new Transformation(this);
	this->collider = NULL;
}

void BaseObject::onCollition(BaseObject* c)
{	
}

BaseObject::~BaseObject()
{
}

BaseObject::BaseObject(vector2d position)
{
	this->position = position;
	this->id = ResourceManager::getInstance()->getNextId();
	this->nombre = "BaseObject";
	this->escala = vector2d(1, 1);
	this->rotacion = 0.0f;
	this->collider = NULL;
}


void BaseObject::OnDraw()
{
}

void BaseObject::OnUpdate()
{
	transform->TransformUpdate();
}


