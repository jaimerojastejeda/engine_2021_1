#include "Vector2d.h"

vector2d::vector2d()
{
	x = 0;
	y = 0;
}

vector2d::~vector2d()
{
}

vector2d::vector2d(float x, float y)
{
	this->x = x;
	this->y = y;
}

float vector2d::magnitud()
{
	float magni_dato = sqrt((x * x) + (y * y));
	return magni_dato;
}

void vector2d::normalizar()
{
	float normal_dato = magnitud();
	x = x / normal_dato;
	y = y / normal_dato;
}

