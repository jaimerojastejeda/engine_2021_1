#pragma once
#include "BaseObject.h"
#include "TextureManager.h"
#include <GL/glew.h>	

class RenderObject : public BaseObject
{
	
	Texture* tex;
	GLuint texId;

public:
	RenderObject(std::string key);
	void virtual OnDraw() override;
	void virtual OnUpdate() override;
};

