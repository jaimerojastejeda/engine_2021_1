#include "Transformation.h"

Transformation::Transformation(BaseObject* owner)
{
	this->owner = owner;

	M_R = new Matrizzz(3, 3);
	M_T = new Matrizzz(3, 3);
	M_S = new Matrizzz(3, 3);
	TransformationGoto = new Matrizzz(3, 3);
	MatrizAcumulada = new Matrizzz(3, 3);
#pragma region MATRIZZZ_IDENTIDAD
	
	for (int f = 0; f < M_S->F; f++)
	{
		for (int c = 0; c < M_S->C; c++)
		{
			if (f == c)
			{
				M_S->Rejas[f][c] = 1;
			}
			else
			{
				M_S->Rejas[f][c] = 0;
			}

		}
	}
	//----------
	for (int f = 0; f < M_T->F; f++)
	{
		for (int c = 0; c < M_T->C; c++)
		{
			if (f == c)
			{
				M_T->Rejas[f][c] = 1;
			}
			else
			{
				M_T->Rejas[f][c] = 0;
			}

		}
	}
	//----------
	for (int f = 0; f < M_R->F; f++)
	{
		for (int c = 0; c < M_R->C; c++)
		{
			if (f == c)
			{
				M_R->Rejas[f][c] = 1;
			}
			else
			{
				M_R->Rejas[f][c] = 0;
			}

		}
	}
	//----------
	for (int f = 0; f < TransformationGoto->F; f++)
	{
		for (int c = 0; c < TransformationGoto->C; c++)
		{
			if (f == c)
			{
				TransformationGoto->Rejas[f][c] = 1;
			}
			else
			{
				TransformationGoto->Rejas[f][c] = 0;
			}

		}
	}
	//----------
	for (int f = 0; f < MatrizAcumulada->F; f++)
	{
		for (int c = 0; c < MatrizAcumulada->C; c++)
		{
			if (f == c)
			{
				MatrizAcumulada->Rejas[f][c] = 1;
			}
			else
			{
				MatrizAcumulada->Rejas[f][c] = 0;
			}
		}
	}
#pragma endregion

}

Transformation::~Transformation()
{
}

void Transformation::SetTransformationMatriz()
{
#pragma region MATRIZZZ_IDENTIDAD

	for (int f = 0; f < M_S->F; f++)
	{
		for (int c = 0; c < M_S->C; c++)
		{
			if (f == c)
			{
				M_S->Rejas[f][c] = 1;
			}
			else
			{
				M_S->Rejas[f][c] = 0;
			}

		}
	}
	//----------
	for (int f = 0; f < M_T->F; f++)
	{
		for (int c = 0; c < M_T->C; c++)
		{
			if (f == c)
			{
				M_T->Rejas[f][c] = 1;
			}
			else
			{
				M_T->Rejas[f][c] = 0;
			}

		}
	}
	//----------
	for (int f = 0; f < M_R->F; f++)
	{
		for (int c = 0; c < M_R->C; c++)
		{
			if (f == c)
			{
				M_R->Rejas[f][c] = 1;
			}
			else
			{
				M_R->Rejas[f][c] = 0;
			}

		}
	}
	//----------
	for (int f = 0; f < TransformationGoto->F; f++)
	{
		for (int c = 0; c < TransformationGoto->C; c++)
		{
			if (f == c)
			{
				TransformationGoto->Rejas[f][c] = 1;
			}
			else
			{
				TransformationGoto->Rejas[f][c] = 0;
			}

		}
	}
#pragma endregion

#pragma region MatrizzzSTR
	//-------------
	M_S->Rejas[0][0] = this->owner->escala.x;
	M_S->Rejas[0][1] = 0;
	M_S->Rejas[0][2] = 0;

	M_S->Rejas[1][0] = 0;
	M_S->Rejas[1][1] = this->owner->escala.y;
	M_S->Rejas[1][2] = 0;

	M_S->Rejas[2][0] = 0;
	M_S->Rejas[2][1] = 0;
	M_S->Rejas[2][2] = 1;
	//-------------
	M_T->Rejas[0][0] = 1;
	M_T->Rejas[0][1] = 0;
	M_T->Rejas[0][2] = owner->position.x;

	M_T->Rejas[1][0] = 0;
	M_T->Rejas[1][1] = 1;
	M_T->Rejas[1][2] = owner->position.y;

	M_T->Rejas[2][0] = 0;
	M_T->Rejas[2][1] = 0;
	M_T->Rejas[2][2] = 1;
	//-------------
	M_R->Rejas[0][0] = (float)cos((3.1416 / 180) * (owner->rotacion));
	M_R->Rejas[0][1] = (float)(sin((3.1416 / 180) * (owner->rotacion))) * (-1);
	M_R->Rejas[0][2] = 0;

	M_R->Rejas[1][0] = (float)sin((3.1416 / 180) * (owner->rotacion));
	M_R->Rejas[1][1] = (float)cos((3.1416 / 180) * (owner->rotacion));
	M_R->Rejas[1][2] = 0;

	M_R->Rejas[2][0] = 0;
	M_R->Rejas[2][1] = 0;
	M_R->Rejas[2][2] = 1;

#pragma endregion
	//el * es lo que esta guardado en esa direccion de memoria
	*TransformationGoto = (*M_T) * ((*M_S) * (*M_R));
	if (owner->parent != nullptr)
	{
		*MatrizAcumulada = (*owner->parent->transform->MatrizAcumulada) * (*TransformationGoto);
	}
	else
	{
		*MatrizAcumulada = *TransformationGoto;
	}
}

void Transformation::TransformUpdate()
{
	this->SetTransformationMatriz();
}

vector2d Transformation::localToGlobal(vector2d xd)
{
	vector2d v = vector2d();
	Matrizzz prueba = Matrizzz(3,1);
	
	prueba = (*MatrizAcumulada) * (xd);

	v.x = prueba.Rejas[0][0];
	v.y = prueba.Rejas[1][0];

	return v;
}
