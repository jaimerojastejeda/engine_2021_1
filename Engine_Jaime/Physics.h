#pragma once
#include "BaseObject.h"
#include<vector>
#include <math.h>

class BaseObject;

enum typeCollider {
	circle,
	rect
};

enum typePhysics {
	p_dynamic,//din�mica
	p_static//est�tica
};

class Collider {
public:
	typePhysics type_p;
	typeCollider type_c;
	float radio, width, height;
	BaseObject* parent;//referencia del base objeect que tiene el collider

public:
	Collider();
	
	bool isCollition(BaseObject* o);	
};

class Physics
{
public:
	int ncollider;
private:
	static Physics* instance;
	std::vector<Collider*> colliders;

private:
	Physics();
	void addObject(Collider* c);
	void removeObject(Collider* c);//container copia y pega xd
	
public:
	~Physics();
	int getIndex(Collider* c);//container copia y pega xd
	void createCircleCollider(BaseObject* parent,float radio, typePhysics type_c);
	void createRectCollider(BaseObject* parent,float width, float height, typePhysics type_c);
	void UpdatePhysics();//te dice constantemente si hay colisi�n o no
	static Physics* getInstance();
};

