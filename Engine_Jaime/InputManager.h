#pragma once
#include <unordered_map>

class InputManager
{
private:
	InputManager();
public:
	~InputManager();
	static InputManager* instance;
	static InputManager* getInstance();
	bool getKeyState(char k);
	void setKeyState(char k, bool _state);


	std::unordered_map<char, bool> keysBool;//tabla hash
};