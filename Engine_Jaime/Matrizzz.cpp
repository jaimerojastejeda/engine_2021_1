#include "Matrizzz.h"
#include <iostream>
Matrizzz::Matrizzz(int x, int y)
{
	this->F = x;
	this->C = y;
}

Matrizzz::~Matrizzz()
{
}

Matrizzz Matrizzz::operator*(Matrizzz uno)
{
    Matrizzz rpta =  Matrizzz(3, 3); 
    for (int i = 0; i < uno.F; i++)
    {
        for (int j = 0; j < uno.C; j++)
        {
            rpta.Rejas[i][j] = (Rejas[i][0] * uno.Rejas[0][j]) + (Rejas[i][1] * uno.Rejas[1][j]) + (Rejas[i][2] * uno.Rejas[2][j]);
        }
    }
    return rpta;  
}

Matrizzz Matrizzz::operator*(vector2d vertice)//sobrecarga de operador
{
    Matrizzz rpta = Matrizzz(3, 1);

    int z = 1;
    for (int i = 0; i < rpta.F; i++)
    {    
        rpta.Rejas[i][0] = (Rejas[i][0] * vertice.x) + (Rejas[i][1] * vertice.y) + (Rejas[i][2] * z);  
    }
    return rpta;
}



