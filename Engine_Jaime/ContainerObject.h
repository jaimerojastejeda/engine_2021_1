#pragma once
#include "BaseObject.h"
#include <vector>
#include<iostream>
class ContainerObject: public BaseObject
{
public:
	int numero_children; //leva la cuenta

private:
	std::vector<BaseObject*> children; //contiene hijos, es la lista

public: 
	ContainerObject();
	~ContainerObject();

	void OnDraw() override;
	void OnUpdate() override;

	int addChild(BaseObject* child);
	int addChildAt(BaseObject* child, int index);
	int removeChild(BaseObject* child);
	int removeChildAt(int index);
	BaseObject* getChild(int index);
	int getIndexChild(BaseObject* child);
	void removeChildren();
};

