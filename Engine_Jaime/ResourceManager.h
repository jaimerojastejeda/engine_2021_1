#pragma once
#include "TextureManager.h"
#include <unordered_map>
#include <string>
class ResourceManager
{
public:
	
	int Width;
	int Height;
	const char* Nombre;
public:
	static ResourceManager* getInstance();
	int getNextId();
	Texture* getTexture(std::string key);

private: 
	ResourceManager();
	~ResourceManager();
	int ids;
	static ResourceManager* instance;
	std::unordered_map<std::string, Texture*>pool_texture;

};

