#include "Physics.h"

Physics* Physics::instance = nullptr;

Physics::Physics() {	
}

Physics::~Physics()
{
}

void Physics::addObject(Collider* c)
{
	this->colliders.push_back(c);
}

void Physics::removeObject(Collider* c)
{
	this->colliders.erase(this->colliders.begin() + (this->getIndex(c)));
}

int Physics::getIndex(Collider* c)
{
	for (int i = 0; i < this->ncollider; i++)
	{
		if (c == colliders[i])
		{
			return i;
		}
	}
	return -1;
}

void Physics::createCircleCollider(BaseObject* parent, float radio, typePhysics type_p = typePhysics::p_static)
{
	Collider* c = new Collider();
	c->radio = radio;
	c->type_c = typeCollider::circle;
	c->type_p = type_p;
	c->parent = parent;
	parent->collider = c;
	this->addObject(c);
}

void Physics::createRectCollider(BaseObject* parent, float width, float height, typePhysics type_p = typePhysics::p_static)
{
	Collider* cr = new Collider();
	cr->width = width;
	cr->height = height;
	cr->type_c = typeCollider::rect;
	cr->type_p = type_p;
	cr->parent = parent;
	parent->collider = cr;
	this->addObject(cr);
}

void Physics::UpdatePhysics()
{
	for (int i = 0; i < colliders.size(); i++)
	{
		if (colliders[i]->type_p == p_dynamic)
		{
			for (int j = 0; j < colliders.size(); j++)
			{
				if (colliders[i]->isCollition(colliders[j]->parent) && i != j)
				{
					colliders[i]->parent->onCollition(colliders[j]->parent);
				}
			}
		}
	}
}

Physics* Physics::getInstance()
{
	if (instance == nullptr) {

		instance = new Physics();
	}

	return instance;
}

Collider::Collider()
{
}

bool Collider::isCollition(BaseObject* o)
{	
	if (this->type_c == circle)
	{
		//referencia con parent
		float cx = o->position.x,
			cy = o->position.y, 
			x= parent->position.x, 
			y= parent->position.y;
		if ((pow((x - cx), 2)) + (pow((y - cy), 2)) < pow(radio, 2))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	if (this->type_c == rect)
	{
		float x = this->parent->position.x, 
			y = this->parent->position.y,
			//el objeto q nos toca es "o" | nosotros.
			cxr = o->position.x + (this->width / 2),
			cxl= o->position.x - (this->width / 2),
			cyt = o->position.y + (this->height / 2),
			cyb= o->position.y - (this->height / 2);
		if (x > cxl && x < cxr && y > cyb && y < cyt)
		{
			return true;
		}	
		else
		{
			return false;
		}
	}
}
