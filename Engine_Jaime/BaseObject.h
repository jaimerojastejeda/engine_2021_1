#pragma once
#include <string>
#include <iostream>
#include "Vector2d.h"
#include "ResourceManager.h"
#include "Transformation.h"
#include "Physics.h"

class Transformation;
class Physics;
class Collider;

class BaseObject
{
public:
	unsigned int id;
	std::string nombre;
	vector2d position;
	vector2d escala;
	float rotacion;
	BaseObject* parent;
	Transformation* transform;
	Collider* collider;
	int width;//relativo a cualquier cosa
	int height;

public:
	BaseObject();
	~BaseObject();
	BaseObject(vector2d position);
	virtual void OnDraw();
	virtual void OnUpdate();

	//funci�n de evento
	virtual void onCollition(BaseObject* c);
};


