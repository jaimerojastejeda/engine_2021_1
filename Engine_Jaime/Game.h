#pragma once
#include <iostream>
#include<thread>
#include<chrono>
#include <cstdlib>
#include "Scene.h"
#include "ResourceManager.h"
#include "InputManager.h"

#include <GL/glew.h>
#include <GL/glut.h>

class Game
{
private:
	Scene* Root; //esta es la escena principal

public:
	int Width, Height;//ancho y alto del juego

public:
	Game();
	~Game();
	Game(int argc, char** argv);
	void SetScene(Scene*);//define la escena
	void Play();//inicia el juego

	void init();
	void display();
	void reshape(int w, int h);//ancho y alto de la pantalla
	void keyboard(unsigned char key, int x, int y);//cuando se presiona
	void UpKeyboard(unsigned char key, int x, int y);//cuando no se est presionando	
	void gameloop();
};



