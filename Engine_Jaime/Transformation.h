#pragma once
#include "BaseObject.h"
#include "Matrizzz.h"
#include <cmath> 
class BaseObject;

class Transformation
{

private:
	BaseObject* owner;
	Matrizzz* M_R;
	Matrizzz* M_S;
	Matrizzz* M_T;
	Matrizzz* TransformationGoto;
	Matrizzz* MatrizAcumulada;

public:
	Transformation(BaseObject* owner);
	~Transformation();
	void SetTransformationMatriz();
	void TransformUpdate();
	vector2d localToGlobal(vector2d xd);
};

