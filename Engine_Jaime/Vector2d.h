#pragma once
#include <cstdio>
#include <cmath>
class vector2d
{
public:

	//datos del vector
	float x;
	float y;

public:
	vector2d();
	~vector2d();
	vector2d(float x, float y);
	float magnitud();
	void normalizar();
};

