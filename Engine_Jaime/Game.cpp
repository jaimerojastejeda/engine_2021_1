#include "Game.h"
Game* g = 0;//el nuevo game que estoy creando

void keyboard_s(unsigned char key, int x, int y)
{
	g->keyboard(key, x, y);
}

void upkeyboard_s(unsigned char key, int x, int y)
{
    g->UpKeyboard(key, x, y);
}

void display_s(void)
{
	g->display();
}

void reshape_s(int w, int h)
{
	g->reshape(w, h);
}

void gameLoop_s(void)
{
	g->gameloop();
}

Game::Game()
{
}

Game::~Game()
{
}

Game::Game(int argc, char** argv)
{
	g = this;
    
    Width = ResourceManager::getInstance()->Width;
    Height = ResourceManager::getInstance()->Height;
    const char* nombre = ResourceManager::getInstance()->Nombre;

    glutInit(&argc, argv);//inicia el opengl
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(Width, Height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(nombre); //nombre de la ventana
    this->init();
    glutDisplayFunc(display_s);
    glutReshapeFunc(reshape_s);
    glutKeyboardFunc(keyboard_s);//fncion al presionar
    glutKeyboardUpFunc(upkeyboard_s);
    glutIdleFunc(gameLoop_s);
}

void Game::SetScene(Scene* root)
{
    this->Root = root;
    this->Root->OnUpdate();
}

void Game::Play()
{
    glutMainLoop();//funci�n qe arranca en todo el opengl
}

void Game::init()
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
}

void Game::display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthMask(GL_FALSE);
    glEnable(GL_TEXTURE_2D);
    this->Root->OnDraw();
    glDisable(GL_TEXTURE_2D);
    glutSwapBuffers();
}

void Game::reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, w, 0, h, 0, 10);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    Width = w;
    Height = h;
}

void Game::keyboard(unsigned char key, int x, int y)//mira si se presiona o no
{
    switch (key) {
        case 27: {
                exit(0);
        }break;

        default: {
            InputManager::getInstance()->setKeyState(key, true);
            //modulo I/O
        }break;
    }    
}

void Game::UpKeyboard(unsigned char key, int x, int y)
{
    switch (key) {
    case 27: {
        exit(0);
    }break;

    default: {
        InputManager::getInstance()->setKeyState(key, false);   
        //modulo I/O
    }break;
    }
}

void Game::gameloop()
{
    //se va while devido a qe el gameloop hac�a eso mismo
        float FPS = 60.0f;//AQU� C�MBIO
        float FrameTime = 1000 / FPS;

        auto starto = std::chrono::system_clock::now();//da el tiempo actual
        
        this->Root->OnUpdate();//actualiza a los objetos
        glutPostRedisplay();//dibujo

        auto endo = std::chrono::system_clock::now();

        auto Duracion = endo - starto;//tiempo transcurrido

        std::chrono::milliseconds prueba((int)(FrameTime));//arreglado, recordar que los frames son segundos exactos

        std::chrono::duration<float, std::milli> RestTime = prueba - Duracion;//el tiempo que me duermo, es el tiempo total que yo espero menos el tiempo que a pasado

        if (RestTime < std::chrono::milliseconds(0))
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        else
        {
            std::this_thread::sleep_for(RestTime);
        }
}
