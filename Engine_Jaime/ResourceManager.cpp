#include "ResourceManager.h"

ResourceManager* ResourceManager::instance = nullptr;

ResourceManager* ResourceManager::getInstance() //retorna la varibale estatica instance
{
	if (instance == nullptr)//si la instancia es nula, entonces creo una instancia
	{
		instance = new ResourceManager();
	}
	return instance;
}

Texture* ResourceManager::getTexture(std::string key)
{
	
	if (pool_texture.find(key) == pool_texture.end())//si llave no se encuentre, cargo la textura
	{
		pool_texture[key] = TextureManager::loadTexture(key.c_str());
	}	
	return pool_texture[key];
}

int ResourceManager::getNextId()
{
	return this->ids++;
}

ResourceManager::ResourceManager()
{
	this->Height = 720;
	this->Width = 1080;
	this->Nombre = "Space Cookies";
	this->ids = 0;
}

ResourceManager::~ResourceManager()
{

}


