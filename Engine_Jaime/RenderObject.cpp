#include "RenderObject.h"

RenderObject::RenderObject(std::string key)//el key es una palabra
{
	//aqu�
	this->tex = ResourceManager::getInstance()->getTexture("imagenes/"+key);
	

	this->width = this->tex->width;
	this->height = this->tex->height;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, this->texId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width, this->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, this->tex->bits);
}

void RenderObject::OnDraw()
{
	glBindTexture(GL_TEXTURE_2D, this->texId);
	
	glBegin(GL_QUADS);
	
	//con respecto al 0,0
	vector2d v1 = vector2d(- (this->width / 2),		- (this->height / 2));
	vector2d v2 = vector2d(- (this->width / 2),  + (this->height / 2));
	vector2d v3 = vector2d(+ (this->width / 2),  + (this->height / 2));
	vector2d v4 = vector2d(+ (this->width / 2),  - (this->height / 2));
	
	v1 = this->transform->localToGlobal(v1);
	v2 = this->transform->localToGlobal(v2);
	v3 = this->transform->localToGlobal(v3);
	v4 = this->transform->localToGlobal(v4);

	glTexCoord2f(0, 0); glVertex3f(v1.x, v1.y, 0);//abajo izq
	glTexCoord2f(0, 1); glVertex3f(v2.x, v2.y, 0);//arriba izq
	glTexCoord2f(1, 1); glVertex3f(v3.x, v3.y, 0);//arriba der
	glTexCoord2f(1, 0); glVertex3f(v4.x, v4.y, 0);//abajo der

	glEnd();
	
}

void RenderObject::OnUpdate()
{
	BaseObject::OnUpdate();
}
