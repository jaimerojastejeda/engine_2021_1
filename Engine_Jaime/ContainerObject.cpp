#include "ContainerObject.h"

ContainerObject::ContainerObject()
{
	this->children = std::vector<BaseObject*>();
	this->numero_children = 0;
}

ContainerObject::~ContainerObject()
{
	this->children.clear();
}

void ContainerObject::OnDraw()
{
	for (int i = 0; i < this->numero_children; i++)
	{
		this->children[i]->OnDraw();
	}
}
	
void ContainerObject::OnUpdate()
{
	//preorden
	BaseObject::OnUpdate();
	for (int i = 0; i < this->numero_children; i++)
	{
		this->children[i]->OnUpdate();
	}
}

int ContainerObject::addChild(BaseObject* child) //agrega el objeto a la lista
{
	this->children.push_back(child);
	child->parent = this; //aqui indica que el hijo tiene como padre al "parent"
	this->numero_children++;
	return this->numero_children;
}

//Guarda un objeto en una posici�n especifica de la lista y devuelve el total de hijos restantes.
int ContainerObject::addChildAt(BaseObject* child, int index)
{	
	this->children.emplace(this->children.begin() + index, child);
	child->parent = this;
	this->numero_children++;
	return this->numero_children;
}

int ContainerObject::removeChild(BaseObject* child) //remueve el objeto de la lista
{
	child->parent = NULL;
	this->children.erase(this->children.begin() + (this->getIndexChild(child)));
	this->numero_children--;
	return this->numero_children;
}

int ContainerObject::removeChildAt(int index) //Remueve el objeto de la lista que tiene ese �ndice
{

	this->children.erase(this->children.begin() + index);
	
	this->numero_children--;
	return this->numero_children;
}

BaseObject* ContainerObject::getChild(int index) //Devuelve el objeto que se encuentra en el �ndice de la lista
{	
	return this->children.at(index);
}

//Devuelve el �ndice donde se encuentra en objeto.(si no se encuentra devuelve -1)
int ContainerObject::getIndexChild(BaseObject* child)
{
	for (int i = 0; i < this->numero_children; i++)
	{
		if (child == children[i])
		{
			return i;
		}
	}
	return -1;
}

void ContainerObject::removeChildren()//elimina a todos los objetos de la lista
{
	this->children.clear();
	this->numero_children = 0;
}